use bitmatrix::BitMatrix;
use log;
use itertools::Itertools;

pub struct RMCode {
    r: u8,
    m: u8,
    matrix: BitMatrix,
    voting_matrices: Vec<BitMatrix>,
}

impl RMCode {
    /// Build the code.
    ///
    /// 1 < r <= m < 255
    ///
    pub fn new(r: u8, m: u8) -> Result<RMCode, String> {
        if r > m { return Err(String::from("r negali būti didesnis už m")) }
        if r == 0 { return Err(String::from("r negali būti lygus 0")) }
        if m == 0 { return Err(String::from("m negali būti lygus 0")) }
        // Matrix rows of the RM(m, r) code.
        // Column count is 2^m, number of rows =  k(m,r).
        let row_len = 2usize.pow(m as u32);
        let mut matrix = BitMatrix::new(m as usize + 1, row_len);
        // Firstly the vectors v_i, 0 ≤ i ≤ m.
        for row in 0..m as usize + 1 {
            for col in generate_row(m, row as u8) {
                matrix.set_bit(row, col, true);
            }
        }
        // Then for each r, 2 <= r <= m build a list of rows derived from v_i (i ≠ 0)
        // rows. Derivation is done with the vedge operation (binary-AND).
        for r in 2..r+1 {
            for combo in (1..m as usize + 1).combinations(r as usize) {
                let row = matrix.rows();
                matrix.add_row(false);
                for col in 0..row_len {
                    if combo.iter().map(|v| matrix[[*v, col]]).all(|x| x) {
                        matrix.set_bit(row, col, true);
                    }
                }
            }
        }

        // Precalculate voting matrices.
        let mut voting_matrices = Vec::new();
        let cmbs = ::std::iter::once(vec![])
        .chain((1..r as usize + 1).flat_map(|r| (0..m).combinations(r))).map(|v| {
                (0..m).filter(move |i| !v.contains(i)).collect_vec()
        });
        for cmb in cmbs {
            let voting_cols = generate_voting_cols(&*cmb, m);
            let mut voting_matrix = BitMatrix::new(row_len, voting_cols.len());
            for (cn, col) in voting_cols.into_iter().enumerate() {
                for (rn, b) in col.into_iter().enumerate() {
                    voting_matrix.set_bit(rn, cn, b);
                }
            }
            voting_matrices.push(voting_matrix);
        }

        // Generate the v_i row of the code matrix
        fn generate_row(m: u8, i: u8) -> impl Iterator<Item=usize> {
            let run_len = 2usize.pow(m as u32 - i as u32);
            let runs = if i == 0 { 1 } else { 2usize.pow(i as u32 - 1) };
            (0..runs).flat_map(move |x| ((x*2*run_len)..).take(run_len))
        }

        // Generate columns of the voting matrix
        fn generate_voting_cols(row: &[u8], m: u8) -> Vec<Vec<bool>> {
            let rowlen = 2usize.pow(m as u32);
            match row.split_first() {
                None => vec![vec![true; rowlen]],
                Some((f, rest)) => {
                    let rest = generate_voting_cols(rest, m);
                    let mut frow = vec![false; rowlen];
                    for i in generate_row(m, f + 1) {
                        frow[i] = true;
                    }
                    rest.iter().map(|x|{
                        x.iter().zip(frow.iter()).map(|(&x, &y)| y&x).collect()
                    }).chain(rest.iter().map(|x|{
                        x.iter().zip(frow.iter()).map(|(&x, &y)| !y&x).collect()
                    })).collect()
                }
            }
        }

        info!("Created an instance of Reed-Muller\nMatrix:\n{:?}\n\nVoting Matrices:", matrix);
        if log_enabled!(log::LogLevel::Info) {
            for (k, vm) in voting_matrices.iter().enumerate() {
                info!("Voting Matrix {}:\n{:?}\n", k, vm);
            }
        }

        Ok(RMCode {
            r: r,
            m: m,
            matrix: matrix,
            voting_matrices: voting_matrices
        })
    }

    /// Length of the encoded vector
    pub fn encode_chunk_length(&self) -> usize {
        self.matrix.rows()
    }

    /// Length of the decoded vector
    pub fn decode_chunk_length(&self) -> usize {
        2usize.pow(self.m as u32)
    }

    /// Encode a single vector
    pub fn encode(&self, v: &BitMatrix) -> super::errors::Result<BitMatrix>
    {
        if v.cols() != self.encode_chunk_length() {
            Err(format!("encoding vector of incorrect length {}", v.cols()).into())
        } else {
            Ok(v.dot_product(&self.matrix))
        }
    }

    /// Decode a single vector
    pub fn decode(&self, mut bs: BitMatrix, af: bool)
    -> super::errors::Result<BitMatrix>
    {
        if bs.cols() != self.decode_chunk_length() {
            return Err(format!("decoding vector of incorrect length {}", bs.cols()).into());
        }
        let rows = self.matrix.rows() - 1;
        let row_len = 2usize.pow(self.m as u32);
        let mut rrowmap = vec![0usize];
        for deg in 1..self.r+1 {
            let prev = rrowmap.last().cloned().unwrap_or(0);
            rrowmap.push(prev + binomial(self.m as u64, deg as u64) as usize);
        }
        let mut outv = BitMatrix::new(1, rows+1);

        for deg in (0..self.r as usize + 1).rev() {
            let (ur, lr) = (rrowmap[deg], if deg == 0 { 0 } else { 1+rrowmap[deg-1] });
            for p in lr..ur+1 {
                debug!("Voting on {:?} with matrix {}\n{:?}", bs, p, self.voting_matrices[p]);
                let votes = bs.dot_product(&self.voting_matrices[p]);
                let ones = votes.storage().iter().fold(0, |a, w| a + w.count_ones());
                let zeros = votes.cols() as u32 - ones;
                debug!("Votes: {:?} ({}/{} ones/zeros)", votes, ones, zeros);
                if af && ones == zeros {
                    debug!("Voting tie! Cannot decode unambiguously!");
                    return Err("cannot unambiguously decode the vector".into());
                }
                outv.set_bit(0, p, if zeros > ones { false } else { true });
            }
            let mut w = BitMatrix::new(1, ur-lr+1);
            for (ci, co) in (lr..ur + 1).enumerate() {
                w.set_bit(0, ci, outv[[0, co]])
            }
            let ms = self.matrix.slice(lr..ur+1, 0..row_len);
            let dp = w.dot_product(&ms);
            bs.xor(&dp);
        }
        Ok(outv)
    }
}

fn binomial(n: u64, k: u64) -> u64 {
    if k == 0 {
        1
    } else if k*2 > n {
        binomial(n,n-k)
    } else {
        let mut e = n-k+1;
        for i in 2..k+1 {
            e = e * (n - k + i);
            e = e / i;
        }
        e
    }
}
