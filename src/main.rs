#![feature(conservative_impl_trait, never_type)]
#![recursion_limit = "1024"]
extern crate clap;
#[macro_use]
extern crate error_chain;
extern crate image;
extern crate rand;
extern crate bitrand;
extern crate itertools;
extern crate bit_vec;
#[macro_use] extern crate log;
extern crate env_logger;

use itertools::Itertools;
use errors::ResultExt;
use clap::{App, Arg, ArgMatches};
use std::path::Path;
use std::io::{Read, Write, BufReader, BufWriter};
use std::fs::File;
use rand::Rng;

mod bitmatrix;
use bitmatrix::*;
mod rmcode;
use rmcode::*;

mod errors {
    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain! {
        foreign_links {
            Image(::image::ImageError);
            Io(::std::io::Error);
            ParseFloat(::std::num::ParseFloatError);
            BitRand(::bitrand::Error);
        }
    }
}

fn main() {
    env_logger::init().unwrap();
    debug!("logging intialized");

    let app = App::new("Reed – Muller")
        .version("1.0")
        .author("Simonas Kazlauskas")
        .about("Reed-Muller encoding and majority decoding")
        .arg(Arg::with_name("error")
             .long("error")
             .help("Error rate in the channel")
             .default_value("0.25")
        )
        .arg(Arg::with_name("input")
             .long("input")
             .default_value("-")
             .short("i")
             .takes_value(true)
             .help("Input file")
        )
        .arg(Arg::with_name("output")
             .long("output")
             .short("o")
             .default_value("-")
             .takes_value(true)
             .help("Output file")
        )
        .arg(Arg::with_name("outvec")
             .long("outvec")
             .short("y")
             .default_value("-")
             .takes_value(true)
             .multiple(true)
             .help("Output file(s) for intermediate outputs")
        )
        .arg(Arg::with_name("file_type")
             .long("file-type")
             .short("t")
             .default_value("bytes")
             .takes_value(true)
             .help("Type of the input file")
             .possible_values(&["vector", "bytes", "image"])
        )
        .arg(Arg::with_name("rm_m")
              .short("m")
              .takes_value(true)
              .help("`m` parameter of the Reed-Muller code")
              .required(true)
        )
        .arg(Arg::with_name("rm_r")
              .short("r")
              .takes_value(true)
              .help("`r` prarameter of the Reed-Muller code")
              .required(true)
        )
        .arg(Arg::with_name("pipeline")
             .required(true)
             .multiple(true)
             .possible_values(&["channel", "decode", "encode", "outvec"])
        ).arg(Arg::with_name("ambig_error")
              .long("fail-on-ambig")
              .short("x")
              .takes_value(false)
        );

    let matches = app.get_matches();
    debug!("arguments matched {:?}", matches);

    if let Err(e) = run_app(matches) {
        let _ = writeln!(::std::io::stderr(), "error: {}", e);
        for e in e.iter().skip(1) {
            let _ = writeln!(::std::io::stderr(), "caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            let _ = writeln!(::std::io::stderr(), "backtrace: {:?}", backtrace);
        }
        ::std::process::exit(1);
    }
}

/// The “main loop” of the program
///
/// matches: the flags passed to the program
/// ret: possible errors
fn run_app<'a>(matches: ArgMatches<'a>) -> errors::Result<()> {
    // Parse most of the flags
    let ifile = matches.value_of("input").expect("input is required but not provided");
    let error = matches.value_of("error").expect("error is required but not provided");
    let error = error.parse::<f64>()?;
    let rm_m = matches.value_of("rm_m").expect("rm_m is required but not provided");
    let rm_r = matches.value_of("rm_r").expect("rm_r is required but not provided");
    let rm_m = rm_m.parse::<u8>().chain_err(|| "could not parse parameter m")?;
    let rm_r = rm_r.parse::<u8>().chain_err(|| "could not parse parameter r")?;
    let mut input = get_input_file(ifile)
        .chain_err(|| format!("cannot open input file `{}`", ifile))?;
    let itype = matches.value_of("file_type").expect("file_type is required but not provided");
    let itype = FileType::from_str(itype).expect("file_type has invalid value");
    let fail_on_ambig = matches.is_present("ambig_error");
    let codec = RMCode::new(rm_r, rm_m)?;
    let ofile = matches.value_of("output").expect("output is required but not provided");
    let mut output = get_output_file(ofile)
        .chain_err(|| format!("cannot open output file `{}`", ofile))?;
    let mut ofiles = matches.values_of("outvec").expect("outvec is required but not provided");
    let pipeline = matches.values_of("pipeline").expect("pipeline not specified");

    let mut expected_vlen = None;

    // Construct a list of tasks.
    //
    // e.g. If the `pipeline` is `encode outvec channel outvec decode` then in order the program
    // will be instructed to do: encoding, output of intermediate vectors, sending via unreliable
    // channel, output of intermediate vectors & decoding.
    let mut pipeline = pipeline.map(|v| match v {
        "channel" => Channel::new(error).map(PipeElement::Channel),
        "decode" => {
            expected_vlen = expected_vlen.or(Some(codec.decode_chunk_length()));
            Ok(PipeElement::Decode(&codec))
        },
        "encode" => {
            expected_vlen = expected_vlen.or(Some(codec.encode_chunk_length()));
            Ok(PipeElement::Encode(&codec))
        }
        "outvec" => {
            let ofn = ofiles.next().unwrap_or_else(|| {
                println!("warning: dest not provided for outvec element. Defaulting to stdout");
                "-"
            });
            get_output_file(ofn).map(PipeElement::Output)
                                .chain_err(|| format!("cannot open output file `{}`", ofn))
        }
        _ => Err("unknown pipeline element".into())
    }).collect::<Result<Vec<_>, _>>().chain_err(|| "could not construct the pipeline")?;

    // Print vector parameters for RM(m, r).
    if let FileType::Vector = itype {
            println!("Length of encoded vector: {}", codec.encode_chunk_length());
            println!("Length of decoded vector: {}", codec.decode_chunk_length());
    }
    if let Some(expected_vlen) = expected_vlen {
        println!("Data is split into vectors of length {}", expected_vlen);
    }

    let ecl = expected_vlen.unwrap_or(codec.encode_chunk_length());
    // Nuskaitome duomenis, juos perleidžiame per užduočių seką ir išvedame galutinius duomenis.
    match itype {
        FileType::Vector => {
            let id = parse_vector(&mut input, ecl)?;
            let od = run_pipeline(id, &mut pipeline, ecl, fail_on_ambig)?;
            write_vector(&mut output, od).chain_err(|| "could not write the output")
        }
        FileType::Bytes => {
            let mut buf = Vec::new();
            input.read_to_end(&mut buf)?;
            let od = run_pipeline(buf, &mut pipeline, ecl, fail_on_ambig)?;
            output.write_all(&od).chain_err(|| "could not write the output")
        }
        FileType::Image => {
            let mut buf = Vec::new();
            input.read_to_end(&mut buf)?;
            let img = image::load_from_memory(&buf)
                .chain_err(|| "could not parse image")?.to_rgba();
            let od = run_pipeline(img, &mut pipeline, ecl, fail_on_ambig)?;
            let encoder = image::png::PNGEncoder::new(output);
            encoder.encode(&od, od.width(), od.height(), image::ColorType::RGBA(8))
                .chain_err(|| "could not write the image")
        }
    }
}

/// Function which executes the list of tasks.
///
/// data: input data in the necessary format (e.g. in case of -tvector, its Vec<BitMatrix>);
/// pipeline: list of tasks;
/// ecl: data is split into vectors of this length;
/// fog: was `-x` specified;
fn run_pipeline<'a, 'b, D>(data: D, pipeline: &mut [PipeElement<'b>], ecl: usize, fog: bool)
-> errors::Result<D>
where D: Data<'a>
{
    let contextual = data.get_contextual();
    let mut outvecs = Vec::new();

    // FIXME: needs a way to go straight to decoding
    for vector in data.iter_vectors(ecl) {
        let mut vector = vector;
        for el in pipeline.iter_mut() {
            match *el {
                PipeElement::Channel(ref mut ch) => ch.mutate_vector(&mut vector),
                PipeElement::Encode(c) => vector = c.encode(&vector)?,
                PipeElement::Decode(c) =>
                    vector = c.decode(vector.clone(), fog)
                    .chain_err(|| format!("could not decode {:?}", vector))?,
                PipeElement::Output(ref mut o) => writeln!(o, "{:?}", vector)?,
            }
        }
        outvecs.push(vector);
    }
    Ok(D::reconstruct(contextual, outvecs))
}

/// Parse input file. If `fname` is `"-"`, stdin is returned.
fn get_input_file<P: AsRef<Path>>(fname: P) -> errors::Result<Box<Read>> {
    /// A wrapper for Windows that handles reading over EOF stuff.
    struct EOFWindowsHandler<R> {
        r: R,
        fused: bool
    }

    impl<R: Read> Read for EOFWindowsHandler<R> {
        fn read(&mut self, buf: &mut [u8]) -> ::std::io::Result<usize> {
            if self.fused {
                Ok(0)
            } else {
                let last = self.r.read(buf)?;
                let eof = buf.iter().position(|&b| b == 0x1a);
                if let Some(eof) = eof {
                    self.fused = true;
                    Ok(eof - 1)
                } else {
                    Ok(last)
                }
            }
        }
    }

    Ok(if fname.as_ref() == Path::new("-") {
        if cfg!(windows) {
            Box::new(EOFWindowsHandler { r: ::std::io::stdin(), fused: false })
        } else {
            Box::new(::std::io::stdin())
        }
    } else {
        Box::new(BufReader::new(File::open(fname)?))
    })
}

/// Get output file. If `fname` is `"-"`, stdout is returned.
fn get_output_file<P: AsRef<Path>>(fname: P) -> errors::Result<Box<Write>> {
    Ok(if fname.as_ref() == Path::new("-") {
        Box::new(::std::io::stdout())
    } else {
        Box::new(BufWriter::new(File::create(fname)?))
    })
}

enum FileType {
    Vector,
    Bytes,
    Image,
}

impl FileType {
    fn from_str(s: &str) -> Option<Self> {
        Some(match s {
            "vector" => FileType::Vector,
            "bytes" => FileType::Bytes,
            "image" => FileType::Image,
            _ => return None,
        })
    }
}

enum PipeElement<'a> {
    Output(Box<Write>),
    Decode(&'a RMCode),
    Encode(&'a RMCode),
    Channel(Channel),
}


/// Channel. Bernoulli distribution is used to generate the noise which is then applied to data
/// with XOR.
struct Channel {
    rng: bitrand::BernoulliRng<rand::ThreadRng>,
}

impl Channel {
    fn new(error: f64) -> errors::Result<Channel> {
        Ok(Channel {
            rng: bitrand::BernoulliRng::new(rand::thread_rng(), error)?,
        })
    }
    fn mutate_vector(&mut self, vec: &mut BitMatrix) {
        unsafe {
            for el in vec.storage_mut() {
                *el ^= self.rng.next_u32();
            }
        }
    }
}

// ------ Data

trait Data<'a> {
    type ContextualData: Clone;
    type VectorIter: Iterator<Item=BitMatrix>;

    /// Gauti tarnybinius duomenis.
    fn get_contextual(&self) -> Self::ContextualData;
    /// Paversti duomenis į vektorius.
    fn iter_vectors(self, chunk: usize) -> Self::VectorIter;
    /// Iš vektorių ir tarnybinių duomenų atgaminti informaciją.
    fn reconstruct(ctx: Self::ContextualData, vecs: Vec<BitMatrix>) -> Self;
}

/// Conversion between vectors and vectors. No contextual data.
impl<'a> Data<'a> for Vec<BitMatrix> {
    type ContextualData = ();
    type VectorIter = ::std::vec::IntoIter<BitMatrix>;

    fn get_contextual(&self) -> () { () }

    fn iter_vectors(self, _: usize) -> Self::VectorIter
    {
        self.into_iter()
    }

    fn reconstruct(_: Self::ContextualData, vecs: Vec<BitMatrix>) -> Self
    {
        vecs
    }
}


/// Read a sequence of 1s and 0s from the file and produce a vector.
fn parse_vector(r: &mut Read, chunk: usize) -> errors::Result<Vec<BitMatrix>> {
    let mut ov = Vec::new();
    let stream = r.bytes().filter(move |c| {
        c.as_ref().map(|&c| c != b' ' && c != b'\n' && c != b'\r').unwrap_or(false)
    });
    for chk in stream.chunks(chunk).into_iter() {
        let mut bv = bit_vec::BitVec::with_capacity(chunk);
        for num in chk {
            match num {
                Ok(b'0') => bv.push(false),
                Ok(b'1') => bv.push(true),
                Err(e) => return Err(e.into()),
                Ok(b) => return Err(format!("invalid input byte for vector: {}", b).into()),
            }
        }
        if bv.len() != chunk {
            return Err(format!("Vector has invalid length (expected {}, got {})",
                               chunk, bv.len()).into());
        }
        ov.push(BitMatrix::from_raw(bv, chunk))
    }
    Ok(ov)
}

/// Write vectors to a file as a sequence of 1s and 0s.
fn write_vector(w: &mut Write, od: Vec<BitMatrix>) -> errors::Result<()> {
    for bits in od.into_iter() {
        for bit in bits.into_iter() {
            write!(w, "{}", if bit { "1" } else { "0" })?;
        }
        write!(w, "\n")?;
    }
    Ok(())
}

/// Conversion between bytes and vectors. Contextual data is the number of bytes.
impl<'a> Data<'a> for Vec<u8> {
    type ContextualData = usize;
    type VectorIter = BytesToVectorsIter;

    fn get_contextual(&self) -> usize { self.len() }

    fn iter_vectors(self, chunk: usize) -> Self::VectorIter
    {
        BytesToVectorsIter {
            bytes: self.into_iter(),
            chunk: chunk,
            byte: 0,
            bit: 0,
            fuse: false,
        }
    }

    fn reconstruct(ctx: Self::ContextualData, vecs: Vec<BitMatrix>) -> Self
    {
        let mut v = vecs_to_bytes(vecs);
        v.truncate(ctx);
        v
    }
}

/// Conversion between images and vectors. Contextual data is the image dimensions.
impl<'a, P> Data<'a> for image::ImageBuffer<P, Vec<u8>>
where P: image::Pixel<Subpixel=u8> + 'static,
{
    type ContextualData = (u32, u32);
    type VectorIter = BytesToVectorsIter;

    fn get_contextual(&self) -> (u32, u32) { (self.width(), self.height()) }

    fn iter_vectors(self, chunk: usize) -> Self::VectorIter
    {
        BytesToVectorsIter {
            bytes: self.into_raw().into_iter(),
            chunk: chunk,
            byte: 0,
            bit: 0,
            fuse: false,
        }
    }

    fn reconstruct(ctx: Self::ContextualData, vecs: Vec<BitMatrix>) -> Self
    {
        let v = vecs_to_bytes(vecs);
        image::ImageBuffer::from_raw(ctx.0, ctx.1, v).unwrap()
    }
}

/// Convert vectors to bytes.
fn vecs_to_bytes(bs: Vec<BitMatrix>) -> Vec<u8> {
    let mut bytes = Vec::new();
    let mut bitout = 8;
    let mut byte = 0;
    for bit in bs.into_iter().flat_map(BitMatrix::into_iter) {
        if bitout == 0 {
            bytes.push(byte);
            byte = 0;
            bitout = 8;
        }
        bitout -= 1;
        if bit {
            byte |= 1 << bitout;
        }
    }
    bytes
}

/// Convert bytes to a sequence of vectors.
struct BytesToVectorsIter {
    bytes: ::std::vec::IntoIter<u8>,
    chunk: usize,
    byte: u8,
    bit: u8,
    fuse: bool,
}

impl BytesToVectorsIter {
    fn next_bit(&mut self) -> Option<bool> {
        if self.bit == 0 {
            self.byte = if let Some(b) = self.bytes.next() {
                b
            } else {
                return None
            };
            self.bit = 8;
        }
        self.bit -= 1;
        Some(self.byte & (1 << self.bit) != 0)
    }
}

impl Iterator for BytesToVectorsIter {
    type Item = BitMatrix;

    fn next(&mut self) -> Option<Self::Item> {
        if self.fuse { return None }
        let mut i = bit_vec::BitVec::from_elem(self.chunk, false);
        for idx in 0..self.chunk {
            if let Some(bit) = self.next_bit() {
                i.set(idx, bit);
            } else {
                self.fuse = true;
                break
            }
        }
        Some(BitMatrix::from_raw(i, self.chunk))
    }
}
