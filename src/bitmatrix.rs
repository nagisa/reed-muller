use bit_vec::{self, BitVec};

/// Bit Matrix.
#[derive(Clone)]
pub struct BitMatrix {
    v: BitVec,
    cols: usize
}

impl BitMatrix {
    /// `rows`*`columns` sized zero matrix.
    pub fn new(rows: usize, columns: usize) -> BitMatrix {
        let bits = columns * rows;
        BitMatrix {
            v: BitVec::from_elem(bits, false),
            cols: columns
        }
    }

    /// Wrap a bit sequence to a matrix.
    pub fn from_raw(bv: BitVec, cols: usize) -> BitMatrix {
        BitMatrix {
            v: bv,
            cols: cols
        }
    }

    /// Set the specified element of the bit matrix.
    pub fn set_bit(&mut self, r: usize, c: usize, v: bool) {
        assert!(c < self.cols);
        assert!(r * c < self.v.len());
        let bit = r * self.cols + c;
        self.v.set(bit, v);
    }

    /// Slice the bit matrix.
    pub fn slice(&self, rs: ::std::ops::Range<usize>, cs: ::std::ops::Range<usize>) -> BitMatrix {
        let mut r = BitMatrix::new(rs.end - rs.start, cs.end - cs.start);
        for (ri, ro) in rs.enumerate() { for (ci, co) in cs.clone().enumerate() {
            r.set_bit(ri, ci, self[[ro, co]]);
        }}
        r
    }

    /// Number of rows in the bitmatrix.
    pub fn rows(&self) -> usize {
        self.v.len() / self.cols
    }

    /// Number of columns in the bitmatrix.
    pub fn cols(&self) -> usize {
        self.cols
    }

    /// Append a row.
    pub fn add_row(&mut self, val: bool) {
        self.v.grow(self.cols, val);
    }

    /// XOR every pair of bits.
    pub fn xor(&mut self, other: &BitMatrix) {
        unsafe {
            for (l, r) in self.v.storage_mut().iter_mut().zip(other.v.storage()) {
                *l ^= *r;
            }
        }
    }

    /// … dot product :).
    pub fn dot_product(&self, other: &BitMatrix) -> BitMatrix {
        assert!(self.rows() == 1);
        assert!(self.cols == other.rows());
        let mut result = other.clone();

        for col in 0..result.cols {
            for row in 0..result.rows() {
                result.set_bit(row, col, self[[0, row]] & other[[row, col]])
            }
            let nb = (0..self.cols).fold(false, |p, row| p ^ result[[row, col]]);
            result.set_bit(0, col, nb);
        }
        result.v.truncate(other.cols);
        result
    }

    pub fn into_iter(self) -> bit_vec::IntoIter {
        self.v.into_iter()
    }

    pub fn storage(&self) -> &[u32] { self.v.storage() }
    pub unsafe fn storage_mut(&mut self) -> &mut Vec<u32> { self.v.storage_mut() }
}

impl ::std::ops::Index<[usize; 2]> for BitMatrix {
    type Output = bool;
    fn index(&self, idx: [usize; 2]) -> &bool {
        let bit = idx[0] * self.cols + idx[1];
        &self.v[bit]
    }
}

impl ::std::fmt::Debug for BitMatrix {
    fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(fmt, "[")?;
        for (bitno, bit) in self.v.iter().enumerate() {
            if bitno != 0 && bitno % self.cols == 0 {
                write!(fmt, "]\n[")?;
            }
            write!(fmt, "{}", if bit { "1" } else { "0" })?;
        }
        write!(fmt, "]")
    }
}
