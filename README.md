An implementation of reed-muller code. Its also an application that makes it easy to check out the
code with plain vectors, bytes or even images.

![An example with three black images](http://i.imgur.com/WLu4Rxw.png)

Run with `-h` to see options:

```
$ cargo run -- -h
Reed – Muller 1.0
Simonas Kazlauskas
Reed-Muller encoding and majority decoding

USAGE:
    kt [FLAGS] [OPTIONS] <pipeline>... -m <rm_m> -r <rm_r>

FLAGS:
    -x, --fail-on-ambig
    -h, --help             Prints help information
    -V, --version          Prints version information

OPTIONS:
        --error <error>            Error rate in the channel [default: 0.25]
    -t, --file-type <file_type>    Type of the input file [default: bytes]  [values: vector, bytes, image]
    -i, --input <input>            Input file [default: -]
    -o, --output <output>          Output file [default: -]
    -y, --outvec <outvec>...       Output file(s) for intermediate outputs [default: -]
    -m <rm_m>                      `m` parameter of the Reed-Muller code
    -r <rm_r>                      `r` prarameter of the Reed-Muller code

ARGS:
    <pipeline>...     [values: channel, decode, encode, outvec]
```

For example to produce the `black-coded.png` from the example above:

```
$ ./kt -m 6 -r 2 -iblack.png -timage encode channel decode --error=0.05 -oblack-coded.png
```

Number of errors in decoded output for different `r` and `m` parameters can be seen in graphs below:

For `r = 1`:

![r=1](http://i.imgur.com/8rNpNPy.png)

For `r = 3`:

![r=3](http://i.imgur.com/H2IxDr5.png)

For `r = 5`:

![r=5](http://i.imgur.com/dR0t5Zw.png)
